This directory should contain the bootstrap scripts/binaries and any official application launchers/scripts.

The bootstrap.py (or compiled equivalent, e.g. bootstrap.so) should live in this directory.

In addition, any other launcher scripts/binaries should be located here as well.

