# README #

This is a meta-repository that demonstrates the standard layout for a Rostra application. 
It includes submodules to the specific Rostra components included. This should be the basis for new Rostra-based projects.

As much as submodules can be a pain, it is recommended that sets of plugins each have their own repository and that 
they are grouped together in a meta-repository like this one. You can also fork this repository and use it as a base.
You would just need to add your own repos as submodules in the plugins directory. Then you just need to create a custom
bootstrap script (i.e. basically just fill in the variables to change the app name, company name, where assets come from, etc.).

Then you're good to go with a complete ready-to-be-commercialized application. At the moment this is GUI focused, but technically,
Rostra is being designed for GUI/CLI applications. The goal of this is that if you have a complex application that uses plugins
to perform computation etc., it makes sense that there might be both a GUI and CLI interface, but it doesn't make sense
that you'd have to duplicate code to get that functionality. (Basically, if your plugins implement all the details, you can use
them equally for a CLI or GUI application. GUI Plugins should always be kept separate from non-GUI plugins to facilitate this 
kind of behavior). 

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact